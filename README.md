# README #

Khemlabs - Kickstarter - Sequelize ORM

### What is this repository for? ###

This lib is intended for developers that are using the khemlabs kickstarter framework, it
is a rapper of the sequelize lib

### Requirements ###

> Khemlabs kickstarter server

### Creating a model ###

Example at server/models/sequelize/Testing.example.js

### Striptags validation ###

By default the lib removes all html tags off all inputs. If you want to add a whitelist, just add the array allowedTags to the column definition in your model.

### Who do I talk to? ###

* dnangelus repo owner and admin
* developer elgambet and khemlabs