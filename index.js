const async     = require('async'),
      seq       = require('sequelize-singleton'),
      log       = require('khem-log'),
      striptags = require('striptags');

(function(){
  seq.discover = [__dirname.replace('node_modules/khem-sequelize-orm', '') + 'server/models/sequelize/'];
  seq.connect(
    __config.DATABASE_NAME, 
    __config.DATABASE_USER, 
    __config.DATABASE_PASSWORD, 
    {
      dialect: __config.DATABASE_DIALECT,
      host: __config.DATABASE_HOST,
      port: __config.DATABASE_PORT,
      logging: __config.DATABASE_LOGGING
    }
  );
})();

module.exports = { 
  
  database: seq.sequelize,
  sequelize: seq.Sequelize,
  models: seq.models,
  modelsNames: Object.keys( seq.models ),
  
  // Add striptags validation
  addMiddlewares() {
    this.modelsNames.forEach( name => {
      this.models[name].beforeCreate( entity => this.striptags(entity) );
      this.models[name].beforeUpdate( entity => this.striptags(entity) );
    });
  },
  
  striptags(entity){
    Object.keys(entity.rawAttributes).forEach( attr => {
      if(typeof entity[attr] == 'string' && isNaN(entity[attr])) {
        entity[attr] = striptags(entity[attr], entity.rawAttributes[attr].allowedTags || []);
      }
    });
  },
  
  /**
  * Replace or add a new association
  *
  * model         {string}    Is the model name, example: Test for Test.model.js
  * plural        {string}    Is the plural name for the model, example: Tests for Test.model.js
  * associations  {object}    Object to associate
  * idName        {string}    The name of the id column of the association table (associations array)
  * instance      {object}    A sequelize instance to associate the ids of the associations array
  */
  updateAssociation1N(data) {
    
    const model           = data.model;
    const association     = data.associations;
    const idName          = data.idName;
    const instance        = data.instance;
    const setAssociation  = 'set' + model;
    
    // It's all runned in a transaction, if one command fails rollback is executed.
    return new Promise( (resolve, reject) => {
      if(association){
        // Update association
        const id = association[idName];
        let search = {where: {}};
        search.where[idName] = id;
        // Find the association in database
        return this.models[model].findOne(search)
          .then( association => {
            return (!association) 
              ? Promise.reject('Association ('+id+') not found') 
              : instance[setAssociation](association);
          })
          .catch( err => Promise.reject('khem-sequelize-orm/index') );
      } else {
        return Promise.reject();
      }
    });
  },
  
  /**
  * Delete all previous associations and insert new ones
  
  * model         {string}    Is the model name, example: Test for Test.model.js
  * plural        {string}    Is the plural name for the model, example: Tests for Test.model.js
  * associations  {array}     A list of objects to associate
  * idName        {string}    The name of the id column of the association table (associations array)
  * instance      {object}    A sequelize instance to associate the ids of the associations array
  */
  updateAssociationsNM(data) {
    
    const model           = data.model;
    const plural          = data.plural;
    const reqAssociations = data.associations;
    const idName          = data.idName;
    const instance        = data.instance;
    const addAssociation  = 'add'     + model;
    const remAssociation  = 'remove'  + model;
    const getAssociations = 'get'     + plural;
    const transaction     = false;
    
    const arrayChanged = (associations, updateAssociations) => {
      const actualIDS = associations.map( association => association[idName] );
      // Search for new associations
      const found = (updateAssociations.findIndex( assoc => actualIDS.indexOf( assoc[idName] ) == -1 ) >= 0 );
      // There no new associations but the user may have removed one.
      return ( found || ( updateAssociations.length < associations.length ) );
    };
    
    const deleteAssociations = deleteAssociations => {
      // Get all actual associations
      return instance[getAssociations]({}, transaction)
        .then( (associations) => {
          if(arrayChanged(associations, deleteAssociations)){
            // Associations changed, updating
            const deletePromises = associations.map( association => instance[remAssociation](association, transaction) );
            return Promise.all(deletePromises);
          } else {
            // Associations did not changed, canceling update
            return Promise.reject();  
          }
        });
    };
    
    const updateAssociations = associations => {
      // This promise return all update promises when finished
      const updatePromises = associations.map( association => {
        return new Promise( (resolve, reject) => {
          const id = association[idName];
          let search = {where: {}};
          search.where[idName] = id;
          // Find the association in database
          return this.models[model].findOne(search, transaction)
            .then( association => {
              return (!association)
                ? Promise.reject('Association ('+id+') not found')
                : instance[addAssociation](association, transaction);
            });
        });
      });
      return Promise.all(updatePromises);
    };
    
    const updateAndDeleteAssociations = t => {
      transaction = {transaction: t};
      return Promise.all([deleteAssociations(reqAssociations), updateAssociations(reqAssociations)])
    };
    
    // Generate all promises needed to execute all database 
    // actions inside a transaction and return them.
    
    // It's all runned in a transaction, if one command fails rollback is executed.
    
    // Generate a transaction and do all querys with it
    return this.database.transaction()
      .then( t => updateAndDeleteAssociations(t) )
      .then( () => t.commit() )
      .catch( () => t.rollback());
  }
  
}
